<?php

namespace App\Console\Command;

use App\DigitalOcean\ClusterProvisioner;
use App\DigitalOcean\ImageProvisioner;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BootstrapCommand extends Command
{
    public function __construct(
        private ImageProvisioner $imageProvisioner,
        private ClusterProvisioner $clusterProvisioner
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('wombat:bootstrap')
            ->setDescription('Bootstrap a Wombat cluster from nothing.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $image = $this->imageProvisioner->provision();
        $this->clusterProvisioner->provision($image);

        return Command::SUCCESS;
    }
}
