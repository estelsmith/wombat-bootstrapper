<?php

namespace App\DigitalOcean;

use App\Docker\AmiBuilder;
use DigitalOceanV2\Entity\Image;

class ImageProvisioner
{
    public function __construct(
        private ImageFinder $imageFinder,
        private AmiBuilder $amiBuilder
    )
    {
    }

    public function provision(): Image
    {
        $image = $this->imageFinder->getNewest();
        if ($image) {
            return $image;
        }

        $amiBuilder = $this->amiBuilder->start();
        $amiProcess = $amiBuilder->getProcess();

        $amiProcess->wait();
        if (!$amiProcess->isSuccessful()) {
            // @TODO Make domain-specific exception to hold process output.
            throw new \RuntimeException('Image failed to build.');
        }

        $image = $this->imageFinder->getNewest();
        if (!$image) {
            throw new \RuntimeException('No image found after build completed.');
        }

        return $image;
    }
}
