<?php

namespace App\DigitalOcean;

use DigitalOceanV2\Client;

class ClientFactory
{
    public function __construct(
        private string $apiToken
    )
    {
    }

    public function create(): Client
    {
        $client = new Client();
        $client->authenticate($this->apiToken);

        return $client;
    }
}
