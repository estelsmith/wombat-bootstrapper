<?php

namespace App\DigitalOcean;

use App\DigitalOcean\Ssh\KeyManager;
use DigitalOceanV2\Client;
use DigitalOceanV2\Entity\Droplet;
use DigitalOceanV2\Entity\Image;
use phpseclib3\Crypt\Common\PrivateKey;
use phpseclib3\Crypt\Common\PublicKey;
use phpseclib3\Exception\UnableToConnectException;
use phpseclib3\Net\SSH2;

class ClusterProvisioner
{
    public function __construct(
        private Client $client,
        private KeyManager $keyManager
    )
    {
    }

    public function provision(Image $image)
    {
        $client = $this->client;
        $sshKey = $this->keyManager->createTemporary();

        $droplet = $this->makeDroplet($image, $sshKey);

        $_[] = new class($client, $droplet) {
            function __construct(
                private Client $client,
                private Droplet $droplet
            ) {}
            function __destruct() {
                dump('Removing droplet ' . $this->droplet->id);
                $this->client->droplet()->remove($this->droplet->id);
            }
        };

        $this->waitForSshConnection($droplet, $sshKey);
    }

    private function makeDroplet(Image $image, PrivateKey $sshKey): Droplet
    {
        $dropletClient = $this->client->droplet();
        /** @var PublicKey $publicKey */
        $publicKey = $sshKey->getPublicKey();
        dump('Creating droplet from image ' . $image->name);
        $droplet = $dropletClient->create(
            names: 'wombat-manager-01',
            region: $image->regions[0],
            size: 's-2vcpu-4gb',
            image: $image->id,
            backups: false,
            sshKeys: [$publicKey->getFingerprint('md5')],
            monitoring: false,
            tags: ['wombat', 'wombat:manager']
        );
        dump('Created droplet ' . $droplet->id);

        $this->waitForDroplet($droplet->id);

        return $dropletClient->getById($droplet->id);
    }

    private function waitForDroplet(string $id)
    {
        dump('Waiting for droplet ' . $id . ' to become active');
        while (true) {
            $poll = $this->client->droplet()->getById($id);
            if ($poll->status === 'active') {
                break;
            }
            sleep(1);
        }
        dump('Droplet ' . $id . ' is active');
    }

    private function waitForSshConnection(Droplet $droplet, PrivateKey $sshKey): SSH2
    {
        // @TODO SshFactory? How much do we care about 'new' here?
        $ssh = new SSH2($this->getDropletIp($droplet));
        $ssh->setTimeout(10);

        while (!$ssh->isAuthenticated()) {
            dump('Trying to connect to droplet ' . $droplet->id);
            try {
                $ssh->login('root', $sshKey);
            } catch (UnableToConnectException $e) {
                $ssh->disconnect();
                sleep(5);
            }
        }
        dump('Connected to droplet ' . $droplet->id);

        return $ssh;
    }

    private function getDropletIp(Droplet $droplet): ?string
    {
        $publicNetwork = null;
        foreach ($droplet->networks as $network) {
            if ($network->type === 'public') {
                $publicNetwork = $network;
                break;
            }
        }

        dump('Droplet ' . $droplet->id . '\'s IP is ' . $publicNetwork?->ipAddress);

        return $publicNetwork?->ipAddress;
    }
}
