<?php

namespace App\DigitalOcean;

use DigitalOceanV2\Client;
use DigitalOceanV2\Entity\Image;
use DigitalOceanV2\ResultPager;

class ImageFinder
{
    public function __construct(
        private Client $client
    )
    {
    }

    /**
     * @return Image[]
     * @throws \DigitalOceanV2\Exception\ExceptionInterface
     */
    public function getAll()
    {
        $pager = new ResultPager($this->client);
        $images = array_filter(
            $pager->fetchAll($this->client->image(), 'getAll', [[
                'private' => true
            ]]),
            fn(Image $image) => str_starts_with($image->name, 'wombat-')
        );

        usort(
            $images,
            fn(Image $a, Image $b) => new \DateTimeImmutable($b->createdAt) <=> new \DateTimeImmutable($a->createdAt)
        );

        return $images;
    }

    public function getNewest(): ?Image
    {
        $images = $this->getAll();
        return reset($images) ?: null;
    }
}
