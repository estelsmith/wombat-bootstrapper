<?php

namespace App\DigitalOcean\Ssh;

use App\Infrastructure\PhpSecLib\Ssh\KeyFactory;
use DigitalOceanV2\Client;
use DigitalOceanV2\Entity\Key;
use phpseclib3\Crypt\Common\PrivateKey;
use phpseclib3\Crypt\Common\PublicKey;

class KeyManager
{
    /**
     * @var Key[]
     */
    private array $temporaryKeys = [];

    public function __construct(
        private KeyFactory $keyFactory,
        private Client $client
    )
    {
    }

    public function createTemporary(): PrivateKey
    {
        $key = $this->keyFactory->create();
        /** @var PublicKey $publicKey */
        $publicKey = $key->getPublicKey();
        $this->temporaryKeys[] = $doKey = $this->client->key()->create(
            'wombat-' . uniqid(),
            $publicKey->toString('openssh')
        );
        dump('Created SSH key ' . $doKey->id . ' - ' . $publicKey->getFingerprint('md5'));

        return $key;
    }

    public function __destruct()
    {
        $keyClient = $this->client->key();
        foreach ($this->temporaryKeys as $key) {
            $keyClient->remove($key->id);
            dump('Deleted SSH key ' . $key->id);
        }
    }
}
