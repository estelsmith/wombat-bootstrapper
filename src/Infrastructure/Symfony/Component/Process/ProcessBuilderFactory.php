<?php

namespace App\Infrastructure\Symfony\Component\Process;

class ProcessBuilderFactory
{
    public function create(): ProcessBuilder
    {
        return new ProcessBuilder();
    }
}
