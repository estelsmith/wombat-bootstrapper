<?php

namespace App\Infrastructure\Symfony\Component\Process;

use Symfony\Component\Process\Process;

class ProcessBuilder
{
    private array $command;
    private ?string $workingDirectory = null;
    private ?array $environment = null;
    private mixed $input = null;
    private ?float $timeout = null;

    public function withCommand(array $command): static
    {
        $clone = clone $this;
        $clone->command = $command;
        return $clone;
    }

    public function withWorkingDirectory(?string $workingDirectory): static
    {
        $clone = clone $this;
        $clone->workingDirectory = $workingDirectory;
        return $clone;
    }

    public function withEnvironment(?array $environment): static
    {
        $clone = clone $this;
        $clone->environment = $environment;
        return $clone;
    }

    public function withInput(mixed $input): static
    {
        $clone = clone $this;
        $clone->input = $input;
        return $clone;
    }

    public function withTimeout(?float $timeout): static
    {
        $clone = clone $this;
        $clone->timeout = $timeout;
        return $clone;
    }

    public function build(): Process
    {
        return new Process(
            $this->command,
            $this->workingDirectory,
            $this->environment,
            $this->input,
            $this->timeout
        );
    }
}
