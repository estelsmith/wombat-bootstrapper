<?php

namespace App\Infrastructure\PhpSecLib\Ssh;

use phpseclib3\Crypt\Common\PrivateKey;
use phpseclib3\Crypt\RSA;

class KeyFactory
{
    public const KEY_LENGTH = 4096;

    public function create(): PrivateKey
    {
        return RSA::createKey(static::KEY_LENGTH);
    }
}
