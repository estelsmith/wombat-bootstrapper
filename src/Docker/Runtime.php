<?php

namespace App\Docker;

use App\Infrastructure\Symfony\Component\Process\ProcessBuilderFactory;

class Runtime
{
    public function __construct(
        private ProcessBuilderFactory $processBuilderFactory
    )
    {
    }

    public function start(ContainerSpec $spec): Container
    {
        $processBuilder = $this->processBuilderFactory->create();

        $command = ['docker', 'run', '--rm', '--name', $spec->getName()];

        if ($spec->getEnvironmentVariables()) {
            foreach ($spec->getEnvironmentVariables() as $environmentVariable) {
                $command[] = '-e';
                $command[] = $environmentVariable->asShellArgument();
            }
        }

        if ($spec->getVolumes()) {
            foreach ($spec->getVolumes() as $volume) {
                $command[] = '-v';
                $command[] = $volume->asShellArgument();
            }
        }

        $command[] = $spec->getImage();

        $process = $processBuilder
            ->withCommand($command)
            ->build()
        ;
        $process->start();

        return new Container($spec, $process);
    }
}
