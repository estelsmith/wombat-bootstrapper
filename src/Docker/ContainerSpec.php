<?php

namespace App\Docker;

use App\Docker\ContainerSpec\EnvironmentVariables;
use App\Docker\ContainerSpec\Volumes;

class ContainerSpec
{
    public function __construct(
        private string $image,
        private string $name,
        private ?EnvironmentVariables $environmentVariables = null,
        private ?Volumes $volumes = null
    )
    {
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentVariables(): ?EnvironmentVariables
    {
        return $this->environmentVariables;
    }

    public function getVolumes(): ?Volumes
    {
        return $this->volumes;
    }
}
