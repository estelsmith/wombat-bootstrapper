<?php

namespace App\Docker;

use Symfony\Component\Process\Process;

class Container
{
    public function __construct(
        private ContainerSpec $spec,
        private Process $process
    )
    {
    }

    public function getSpec(): ContainerSpec
    {
        return $this->spec;
    }

    public function getProcess(): Process
    {
        return $this->process;
    }
}
