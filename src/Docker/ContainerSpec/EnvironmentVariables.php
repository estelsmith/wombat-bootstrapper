<?php

namespace App\Docker\ContainerSpec;

/**
 * @method EnvironmentVariable current
 */
class EnvironmentVariables extends \IteratorIterator
{
    /**
     * @var EnvironmentVariable[]
     */
    private iterable $items;

    /**
     * EnvironmentVariables constructor.
     * @param EnvironmentVariable[] $items
     */
    public function __construct(iterable $items = [])
    {
        $this->items = new \ArrayIterator();
        if ($items) {
            $this->add(...$items);
        }

        parent::__construct($this->items);
    }

    public function add(EnvironmentVariable $item, EnvironmentVariable ...$extraItems)
    {
        $this->items->append($item);
        foreach ($extraItems as $extraItem) {
            $this->items->append($extraItem);
        }
    }
}
