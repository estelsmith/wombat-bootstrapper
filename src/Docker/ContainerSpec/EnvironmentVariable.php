<?php

namespace App\Docker\ContainerSpec;

class EnvironmentVariable
{
    public function __construct(
        private string $name,
        private string $value
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function asShellArgument(): string
    {
        return sprintf(
            '%s=%s',
            $this->name,
            $this->value
        );
    }
}
