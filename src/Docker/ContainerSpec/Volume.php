<?php

namespace App\Docker\ContainerSpec;

class Volume
{
    public function __construct(
        private string $hostPath,
        private string $containerPath
    )
    {
    }

    public function getHostPath(): string
    {
        return $this->hostPath;
    }

    public function getContainerPath(): string
    {
        return $this->containerPath;
    }

    public function asShellArgument(): string
    {
        return sprintf(
            '%s:%s',
            $this->hostPath,
            $this->containerPath
        );
    }
}
