<?php

namespace App\Docker\ContainerSpec;

/**
 * @method Volume current
 */
class Volumes extends \IteratorIterator
{
    /**
     * @var Volume[]
     */
    private iterable $items;

    /**
     * @param Volume[] $items
     */
    public function __construct(iterable $items = [])
    {
        $this->items = new \ArrayIterator();
        if ($items) {
            $this->add(...$items);
        }

        parent::__construct($this->items);
    }

    public function add(Volume $item, Volume ...$extraItems)
    {
        $this->items->append($item);
        foreach ($extraItems as $extraItem) {
            $this->items->append($extraItem);
        }
    }
}
