<?php

namespace App\Docker;

use App\Docker\ContainerSpec\EnvironmentVariable;
use App\Docker\ContainerSpec\EnvironmentVariables;

class AmiBuilder
{
    public function __construct(
        private Runtime $runtime,
        private string $apiToken
    )
    {
    }

    public function start(): Container
    {
        return $this->runtime->start(
            new ContainerSpec(
                image: 'wombat-ami-builder',
                name: sprintf('wombat-%s', uniqid()),
                environmentVariables: new EnvironmentVariables([
                    new EnvironmentVariable('PKR_VAR_skip_updates', '1'),
                    new EnvironmentVariable('PKR_VAR_digitalocean_token', $this->apiToken)
                ])
            )
        );
    }
}
